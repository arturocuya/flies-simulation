Proyecto final de CS1100
Por Arturo Cuya

Nombre del Proyecto: Simulación del movimiento de un enjambre de moscas

Enlace a Github: https://github.com/arturocuya/simulacion_moscas

Descripción:

Nuestro proyecto toma la tarea de simular visualmente el recorrido de un enjambre de moscas desde un punto de partida hasta un objetivo, ambos pudiendo ser predefinidos por el usuario. 
Las moscas no saben exactamente donde se encuentra su objetivo, sino que van percibiendo un olor que guía sus movimientos, lo que les permite aumentar la tendencia de cierto movimiento si se incrementa el olor en esa dirección o corregir su comportamiento si perciben menos olor. De esta manera el enjambre puede partir desde cualquier punto y llegar a un objetivo situado en cualquier otro.
Detalles:

Al principio del programa se abre una ventana donde el usuario puede seleccionar con clic derecho la posición inicial del enjambre y la posición del objetivo a seguir. Luego el usuario debe pulsar enter y comenzará la simulación.
Todas las moscas parten del punto focal seleccionado por el usuario con una probabilidad aleatoria de ir en cada una de las cuatro direcciones cardinales. Con la función smell() la mosca recibe un número que le permite saber si el olor del objetivo aumenta o disminuye con respecto a su posición anterior.

La función smell() se calcula así:

smell = 1 / sqrt(∆x^2 + ∆y)

         Donde    ∆x:diferencia entre la posición x de la mosca y la del objetivo

                  ∆y:diferencia entre la posición y de la mosca y la del objetivo

Usando la información de la función smell(), la mosca puede incrementar o reducir su tendencia a ir hacia cierta dirección. La probabilidad de ir en cada una de las cuatro direcciones se guarda en una lista, y las direcciones tomadas por las moscas se guarda en otra. Si el olor aumentó en la última dirección tomada con respecto a la anterior, la probabilidad de ir en ella aumenta. Aparte de todo esto, un 20% de las veces las moscas no siguen su instinto, sino que se mueven de una forma completamente aleatoria. Esto es para imitar con más exactitud el movimiento impredecible de las moscas en la realidad.

Una vez que una mosca llega a su objetivo, se queda pegado a este. Eventualmente todas las moscas llegarán a su meta sin importar que tan cerca o lejos se hayan posicionado el foco del enjambre y el objetivo.
